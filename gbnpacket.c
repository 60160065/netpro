/* gbnpacket.c - กำหนดโครงสร้างแพ็กเก็ต go-back-n
 * by Elijah Jordan Montgomery <elijah.montgomery@uky.edu>
 */
struct gbnpacket
{
  int type;
  int seq_no;
  int length;
  char data[512];
};
