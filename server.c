/* server.c - การติดตั้งเซิร์ฟเวอร์ go-back-n ใน C
 * by Elijah Jordan Montgomery <elijah.montgomery@uky.edu>
 * based on code by Kenneth Calvert
 *
 * This implements a go-back-n server that implements reliable data 
 * transfer over UDP using the go-back-n ARQ with variable chunk size
 *
 * for debug purposes, a loss rate can also be specified
 * compile with "gcc -o server server.c"
 * tested on UKY CS Multilab 
 */
 /* #include ไฟล์ คือการเอา source code ของไฟล์เหล่านั้นมาวางไว้ที่บรรทัดนั้นๆ ก่อนทำการ compile*/
#include <stdio.h>		/* for printf() and fprintf() */
#include <sys/socket.h>		/* for socket() and bind() */
#include <arpa/inet.h>		/* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>		/* for atoi() and exit() */
#include <string.h>		/* for memset() */
#include <unistd.h>		/* for close() */
#include <errno.h>
#include <memory.h>
#include <signal.h>
#include "gbnpacket.c"		/* กำหนดโครงสร้างแพ็กเก็ต go-back-n */
/*  #define ใช้ในการแทนที่ */
#define ECHOMAX 255		/* สตริงที่ยาวที่สุดเพื่อสะท้อน */

void DieWithError (char *errorMessage);	/* ฟังก์ชันการจัดการข้อผิดพลาดภายนอก */
void CatchAlarm (int ignored);

int
main (int argc, char *argv[])
{
  char buffer[8193];		/* buffer กับห้องพัก(array)สำหรับไบต์ว่าง (null byte) --buffer คือ ที่พักข้อมูล เป็นพื้นที่บนหน่วยความจำใช้สำหรับเก็บข้อมูลชั่วคราวขณะถูกย้ายจากแหล่งหนึ่งไปอีกแหล่งหนึ่ง */ 
  buffer[8192] = '\0';		/* ค่าว่าง (null) ยกเลิก buffer*/
  int sock;			/* Socket คือ กลุ่มของหมายเลข Port และ หมายเลข IP ซึ่งจะเป็นตัวบ่งชี้ที่เฉพาะเจาะจงสำหรับ Network process*/
  struct sockaddr_in gbnServAddr;	/* Local address */
  struct sockaddr_in gbnClntAddr;	/* Client address */
  unsigned int cliAddrLen;	/* ความยาวของข้อความที่เข้ามา */
  unsigned short gbnServPort;	/* Server port */
  int recvMsgSize;		/* ขนาดของข้อความที่ได้รับ */
  int packet_rcvd = -1;		/* ได้รับแพ็กเก็ตสูงสุดสำเร็จแล้ว */
  struct sigaction myAction;
  double lossRate;		/* อัตราการสูญเสียเป็นทศนิยม 50% = input. 50 */


  bzero (buffer, 8192);		/* zero out the buffer*/

  if (argc < 3 || argc > 4)		/* ทดสอบจำนวนพารามิเตอร์ที่ถูกต้อง */
    {
      fprintf (stderr, "Usage:  %s <UDP SERVER PORT> <CHUNK SIZE> [<LOSS RATE>]\n", argv[0]);
      exit (1);
    }

  gbnServPort = atoi (argv[1]);	/* ARG แรก:  local port */
  int chunkSize = atoi(argv[2]);	/*จำนวนไบต์ต่ออัน */
  if(argc == 4)
	lossRate = atof(argv[3]);	/* อัตราการสูญเสียเป็นเปอร์เซ็นต์เช่น 50% = .50 */
  else
	lossRate = 0.0;			/*ค่าเริ่มต้นเป็น 0% อัตราการสูญเสียโดยเจตนา */

  srand48(123456789);/* เมล็ดกำเนิดตัวเลขเทียมหลอก */
  /* สร้างซ็อกเก็ตสำหรับการส่ง / รับดาตาแกรม */
  if ((sock = socket (PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    DieWithError ("socket() failed");

  /* สร้างโครงสร้าง local address */
  memset (&gbnServAddr, 0, sizeof (gbnServAddr));	/* ไม่มีโครงสร้าง */
  gbnServAddr.sin_family = AF_INET;	/* ตระกูลที่อยู่อินเทอร์เน็ต */
  gbnServAddr.sin_addr.s_addr = htonl (INADDR_ANY);	/* อินเทอร์เฟซที่เข้ามาใด ๆ */
  gbnServAddr.sin_port = htons (gbnServPort);	/* Local port */

  /* ผูกกับ local address */
  if (bind (sock, (struct sockaddr *) &gbnServAddr, sizeof (gbnServAddr)) <
      0)
    DieWithError ("bind() failed");
  myAction.sa_handler = CatchAlarm;
  if (sigfillset (&myAction.sa_mask) < 0)/*ตั้งค่าทุกอย่างสำหรับตัวจับเวลา - ใช้สำหรับการขาดหายเท่านั้น */
    DieWithError ("sigfillset failed");
  myAction.sa_flags = 0;
  if (sigaction (SIGALRM, &myAction, 0) < 0)
    DieWithError ("sigaction failed for SIGALRM");
  for (;;)			/* Run ตลอดการ */
    {
      /* กำหนดขนาดของพารามิเตอร์เข้า - ออก */
      cliAddrLen = sizeof (gbnClntAddr);
      struct gbnpacket currPacket; /* แพ็คเก็ตปัจจุบันที่เรากำลังทำงานด้วย */
      memset(&currPacket, 0, sizeof(currPacket));
      /* บล็อกจนกว่าจะได้รับข้อความจาก client */
      recvMsgSize = recvfrom (sock, &currPacket, sizeof (currPacket), 0, /* รับแพ็คเก็ต GBN */
			      (struct sockaddr *) &gbnClntAddr, &cliAddrLen);
      currPacket.type = ntohl (currPacket.type);
      currPacket.length = ntohl (currPacket.length); /* แปลงจากเครือข่ายเป็นลำดับไบต์ของโฮสต์ */
      currPacket.seq_no = ntohl (currPacket.seq_no);
      if (currPacket.type == 4) /* ข้อความขาดหาย */
	{
	  printf ("%s\n", buffer);
	  struct gbnpacket ackmsg;
	  ackmsg.type = htonl(8);
	  ackmsg.seq_no = htonl(0);/*แปลงเป็นเครือข่าย endianness */
	  ackmsg.length = htonl(0);
	  if (sendto
	      (sock, &ackmsg, sizeof (ackmsg), 0,
	       (struct sockaddr *) &gbnClntAddr,
	       cliAddrLen) != sizeof (ackmsg))
	    {
	      DieWithError ("Error sending tear-down ack"); /*ไม่ใช่ข้อมูลขนาดใหญ่ที่ได้รับแล้ว */
	    }
	  alarm (7);
	  while (1)
	    {
	      while ((recvfrom (sock, &currPacket, sizeof (int)*3+chunkSize, 0,
				(struct sockaddr *) &gbnClntAddr,
				&cliAddrLen))<0)
		{
		  if (errno == EINTR)	/* Alarm ดับลง  */
		    {
		      /* ไม่เคยไปถึง */
		      exit(0);
		    }
		  else
		    ;
		}
	      if (ntohl(currPacket.type) == 4) /* โต้ตอบต่อข้อความที่ขาดหายมากขึ้น */
		{
		  ackmsg.type = htonl(8);
		  ackmsg.seq_no = htonl(0);/* แปลงเป็นเครือข่าย endianness */
		  ackmsg.length = htonl(0);
		  if (sendto
		      (sock, &ackmsg, sizeof (ackmsg), 0, /* ส่งการขาดหายรับทราบ */
		       (struct sockaddr *) &gbnClntAddr,
		       cliAddrLen) != sizeof (ackmsg))
		    {
		      DieWithError ("Error sending tear-down ack");
		    }


		}


	    }
	  DieWithError ("recvfrom() failed");
	}
      else
	{
	  if(lossRate > drand48())
		continue; /* drop packet - สำหรับการทดสอบ / การเดบิต */
	  printf ("---- RECEIVE PACKET %d length %d\n", currPacket.seq_no, currPacket.length);

	  /* ส่งรับทราบและเก็บไว้ในบัฟเฟอร์ */
	  if (currPacket.seq_no == packet_rcvd + 1)
	    {
	      packet_rcvd++;
	      int buff_offset = chunkSize * currPacket.seq_no;
	      memcpy (&buffer[buff_offset], currPacket.data, /* คัดลอกข้อมูลแพ็คเก็ตไปยังบัฟเฟอร์ */
		      currPacket.length);
	    }
	  printf ("---- SEND ACK %d\n", packet_rcvd);
	  struct gbnpacket currAck; /* ยอมรับแพ็คเก็ต */
	  currAck.type = htonl (2); /* แปลงเป็นลำดับไบต์ของเครือข่าย */
	  currAck.seq_no = htonl (packet_rcvd);
	  currAck.length = htonl(0);
	  if (sendto (sock, &currAck, sizeof (currAck), 0, /* ส่งรับทราบ */
		      (struct sockaddr *) &gbnClntAddr,
		      cliAddrLen) != sizeof (currAck))
	    DieWithError
	      ("sendto() sent a different number of bytes than expected");
	}
    }
  /* ไม่ถึง*/
}

void
DieWithError (char *errorMessage)
{
  perror (errorMessage);
  exit (1);
}

void
CatchAlarm (int ignored) /* ออกเมื่อพบการหมดเวลา */
{
  exit(0);
}
